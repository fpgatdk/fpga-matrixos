#include "dirac.h"

int main( int argc, char *argv[] )
{
	int mode;
	if ( strcmp(argv[4],"int") == 0){
		mode = INTFILE;
	} else {
		mode = COMPLEXFILE;
	}
	readVek(argv[1],mode);
	readMat1(argv[2],mode);
	readMat2(argv[3],mode);

	int i, j, k, l, clock;

	for(i=0; i < N; i++){for(j=0; j < N; j++){
/*		for(k=0; k < 3; k++)
		{
			latt[i][j].vn[k] = 0;
		}

		for(k=0; k < ADDMEM_LENGTH; k++)
		{
			latt[i][j].addmem[k] = 0;
		}
*/
		latt[i][j].counter = 0;
		
	}}

	for(clock=1; clock < STEP_TIME + 1; clock++)
	{
		/*
		printf("%d %d %d %d\n",latt[0][0].vn[0],latt[0][0].vn[1],latt[0][0].vn[2],clock);
		for(k=113;k<137;k++)
		{
			printf("%d ",latt[0][0].addmem[k]);
		}
		printf("\n");
		*/
		for(i=0; i < N; i++)
		{
			for(j=0; j < N; j++)
			{
				step(i,j);
			}
		}
	}

	printf("Mass parameter\n\nm = %d\n",M);

	printf("First U matrix\n\n");

	for(i=0; i < N; i++){for(j=0; j < N; j++){
		for(k=0; k < 3; k++)
		{
			for(l=0; l < 3; l++){
				printf("%d %d %d %d (%d)\n",i,j,k,l,latt[i][j].ua[k][l]);
			}
			printf("\n");
		}
		printf("\n");
	}}
	printf("Second U matrix\n\n");

	for(i=0; i < N; i++){for(j=0; j < N; j++){
		for(k=0; k < 3; k++)
		{
			for(l=0; l < 3; l++){
				printf("%d %d %d %d (%d)\n",i,j,k,l,latt[i][j].ub[k][l]);
			}
			printf("\n");
		}
		printf("\n");
	}}

	printf("Initial vector\n\n");

	for(i=0; i < N; i++){for(j=0; j < N; j++){
		for(k=0; k < 3; k++)
		{
			printf("%d %d %d (%d)\n",i,j,k,latt[i][j].v[k]);
		}
		printf("\n");
	}}

	printf("Output vector\n\n");

	for(i=0; i < N; i++){for(j=0; j < N; j++){
		for(k=0; k < 3; k++)
		{
			printf("%d %d %d (%d)\n",i,j,k,latt[i][j].vn[k]);
		}
		printf("\n");
	}}
/*
	printf("ua\n");
	for(i=0; i < 3; i++)
	{
		printf("%+3d %+3d %+3d\n",latt[0][0].ua[i][0],latt[0][0].ua[i][1],latt[0][0].ua[i][2]);
	}
	printf("\n");
	printf("ub\n");
	for(i=0; i < 3; i++)
	{
		printf("%+3d %+3d %+3d\n",latt[0][0].ub[i][0],latt[0][0].ub[i][1],latt[0][0].ub[i][2]);
	}
	printf("\n");
	printf("uc\n");
	for(i=0; i < 3; i++)
	{
		printf("%+3d %+3d %+3d\n",latt[0][0].uc[i][0],latt[0][0].uc[i][1],latt[0][0].uc[i][2]);
	}
	printf("\n");
	printf("ud\n");
	for(i=0; i < 3; i++)
	{
		printf("%+3d %+3d %+3d\n",latt[0][0].ud[i][0],latt[0][0].ud[i][1],latt[0][0].ud[i][2]);
	}
	printf("\n");
	*/
	return(0);
}
