#include <stdio.h>

#define N 4 //a negyzethalo merete
#define M 1 //m parameter

typedef int number; //ha kesobb short/long kell helyette
typedef number vektor[3]; //vektor
typedef number matrix[3][3]; //matrix

#define ADDMEM_LENGTH 200//pa memoriaja
typedef struct {
	vektor v,vn;
	matrix ua,ub,uc,ud;
	number addmem[ADDMEM_LENGTH];
	number counter;
} pa; //principal agent

pa latt[N][N]; //lattice, minden pontban egy pa

// -- Alap muveletek --

//ket szam osszeget beirja harmadikba
void plus (number *a, number *b, number *c);

//balshift
void shiftl (number *a);

//jobbshift
void shiftr (number *a);

// -- Osszetett muveletek --

//egy szamot harom masikkal egyszerre szoroz
void parallelProd (number *a, number *b1, number *b2, number *b3,
	number *memstart, number *out1, number *out2, number *out3);

//vektort matrixxal szoroz
void matrixProd (vektor v, matrix m, number *memstart,
	number *out1, number *out2, number *out3);
