/*
 * A PROGRAM INTEGER OUTPUT VEKTORT AD VISSZA, INTEGEREKKEL VÉGEZ MŰVELETEKET.
 * OLVAS INT ÉS COMPLEX ALAPÚ FILE-T IS. UTÓBBI ESETÉN A VALÓS RÉSZEK SZORZATAINAK 
 * KEREKÍTETTJEIVEL DOLGOZIK.
 * JELEN ÁLLAPOTÁBAN INT FILEOKON DOLGOZIK, EZT KIKOMMENTELÉSSEL LEHET MÓDOSÍTANI
 * A MAIN FÜGGVÉNYBEN.
 * CURRENT INPUTS:
 *  -> input_vec_modified 
    -> input_mat1_modified
    -> input_mat2_modified 
 * CURRENT OUPUT:
    -> output_vec
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>

/* lattice will be N x N */
#define N 4
#define MAG 10
#define COMPLEXFILE 1
#define INTFILE 2

/* initial vector */
//changed from complex to int
int vi[N][N][3];    

/* output vector */
//changed from complex to int
int vo[N][N][3];    

/* first U matrix */
//changed from complex to int
int u1[N][N][3][3];  

/* second U matrix */
//changed from complex to int
int u2[N][N][3][3];  

/* mass parameter */
//multiplied with the magnifying factor / changed from complex to int
int m = 0.1*MAG;           



/* set up initial vector */
//changed to store magnified real part only 
//reads complex or int files
void set_vec( char* name, int mod )
{
    FILE * f;
    f = fopen( name, "r" );
    int i, j, a, i2, j2, a2;
    double re, im;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                switch(mod) {
                    case COMPLEXFILE:
                        fscanf( f, "%d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &re, &im );
                        vi[i2][j2][a2] = round(re*MAG);
                        break;
                    case INTFILE:
                        fscanf( f, "%d %d %d (%lf)\n", &i2, &j2, &a2, &re);
                        vi[i2][j2][a2] = round(re);
                        break;
                    default:
                        printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                }                         
                //original line: vi[i2][j2][a2] = re + I * im;
                 
            }
        }
    }
    fclose( f );
}

/* save output vector */
//saves only integer vector to file
void save_vec( char* name , int v[N][N][3])
{
    FILE * f;
    f = fopen( name, "w" );
    int i, j, a;
    double re; 
    //original line: double im;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {              
                //original line: im = cimag( vo[i][j][a] ); 
                //orinial line: fprintf( f, "%d %d %d (%d,%d)\n", i, j, a, re, im );
                fprintf( f, "%d %d %d (%d)\n", i, j, a, v[i][j][a] );
            }
        }
    }
    fclose( f );
}

//saves integer matrix to file
void save_mat( char* name , int mat[N][N][3][3])
{
    FILE * f;
    f = fopen( name, "w" );
    int i, j, a,b;
    double re; 
    //original line: double im;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {              
                for( b = 0; b < 3; b++ )
                {                          
                        fprintf( f, "%d %d %d %d (%d)\n", i, j, a, b, mat[i][j][a][b] );
                }
            }
        }
    }
    fclose( f );
}



/* set up first U matrix */
//changed to store only the magnified real parts as integers
void set_mat1( char* name, int mod )
{
    FILE * f;
    int i, j, a, b, i2, j2, a2, b2;
    double re, im; //im IS UNUSED HERE!
    f = fopen( name, "r" );
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                for( b = 0; b < 3; b++ )
                {
                    switch(mod) {
                        case COMPLEXFILE:
                            fscanf( f, "%d %d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &b2, &re, &im );
                            u1[i2][j2][a2][b2] = round(re*MAG);
                            break;
                        case INTFILE:
                            fscanf( f, "%d %d %d %d (%lf)\n", &i2, &j2, &a2, &b2, &re );
                            u1[i2][j2][a2][b2] = round(re);
                            break;
                        default:
                            printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                    }                                              
                    //original line: u1[i2][j2][a2][b2] = re + I * im;
                    
                }
            }
        }
    }
    fclose( f );
}

/* set up second U matrix */
//changed to store only the magnified real parts as integers
void set_mat2( char* name, int mod )
{
    FILE * f;
    int i, j, a, b, i2, j2, a2, b2;
    double re, im; //im IS UNUSED HERE
    f = fopen( name, "r" );
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                for( b = 0; b < 3; b++ )
                {
                    switch(mod) {
                        case COMPLEXFILE:
                            fscanf( f, "%d %d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &b2, &re, &im );
                            u2[i2][j2][a2][b2] = round(re*MAG);
                            break;
                        case INTFILE:
                            fscanf( f, "%d %d %d %d (%lf)\n", &i2, &j2, &a2, &b2, &re );
                            u2[i2][j2][a2][b2] = round(re);
                            break;
                        default:
                            printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                    }                   
                    //original line: u2[i2][j2][a2][b2] = re + I * im;
                    
                }
            }
        }
    }
    fclose( f );
}

/* product of 3x3 complex matrix and 3-component complex vector */
//changed to compute the product of 3x3 integer matrix and a 3-component integer vector
//original line: void mat_prod( complex (* wo)[3], complex mat[3][3], complex wi[3] )
void mat_prod( int (* wo)[3], int mat[3][3], int wi[3] )
{
    int a, b;

    for( a = 0; a < 3; a++ )
    {
        //original line: (*wo)[a] = 0.0 + 0.0 * I;
        (*wo)[a] = 0;
        for( b = 0; b < 3; b++ )
        {
            (*wo)[a] += mat[a][b] * wi[b];
        }
    }
}

/* product of adjoint of 3x3 complex matrix and 3-component complex vector */
//changed to compute the product of 3x3 transposed integer matrix and a 3-component integer vector
//original line: void mat_prod( complex (* wo)[3], complex mat[3][3], complex wi[3] )
void mat_prod_adj( int (* wo)[3], int mat[3][3], int wi[3] )
{
    int a, b;

    for( a = 0; a < 3; a++ )
    {
        //original line: (*wo)[a] = 0.0 + 0.0 * I;
        (*wo)[a] = 0;
        for( b = 0; b < 3; b++ )
        {
            (*wo)[a] +=  mat[b][a]  * wi[b];
        }
    }
}

/* multiplying a lattice vector by the Dirac operator */
//complex temp variables changed to int, operations are alredy integer based
void dirac( void )
{
    int a, i, j, ip, im, jp, jm;
/*
 * original lines:
    complex tmp1[3];
    complex tmp2[3];
    complex tmp3[3];
    complex tmp4[3];
*/
    int tmp1[3];
    int tmp2[3];
    int tmp3[3];
    int tmp4[3];
    for( i = 0; i < N; i++ )
    {
        ip = ( i + 1 ) % N;
        im = ( i + N - 1 ) % N;
        for( j = 0; j < N; j++ )
        {
            jp = ( j + 1 ) % N;
            jm = ( j + N - 1 ) % N;
            mat_prod( &tmp1, u1[i][j], vi[ip][j] );
            mat_prod( &tmp2, u2[i][j], vi[i][jp] );
            mat_prod_adj( &tmp3, u1[im][j], vi[im][j] );
            mat_prod_adj( &tmp4, u2[i][jm], vi[i][jm] );
            for( a = 0; a < 3; a++ )
            {
                vo[i][j][a] = m * vi[i][j][a];
                vo[i][j][a] += tmp1[a];
                vo[i][j][a] += tmp2[a];
                vo[i][j][a] -= tmp3[a];
                vo[i][j][a] -= tmp4[a];
            }
        }
    }
}

/* print a lattice vector */
//changed to print the integer lattice vector
//original line: void print_vec( complex v[N][N][3] )
void print_vec( int v[N][N][3] )
{
    int i, j, a;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                //original line: printf( "%d %d %d (%d,%d)\n", i, j, a, creal(v[i][j][a]), cimag(v[i][j][a]) );
                printf( "%d %d %d (%d)\n", i, j, a, v[i][j][a] );
            }
            printf( "\n" );
        }
    }
}

/* print a lattice matrix */
//changed to print the integer lattice matrix
//original line: void print_u( complex u[N][N][3][3] )
void print_u( int u[N][N][3][3] )
{
    int i, j, a, b;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                for( b = 0; b < 3; b++ )
                {
                    //original line: printf( "%d %d %d %d (%d,%d)\n", i, j, a, b, creal( u[i][j][a][b] ), cimag( u[i][j][a][b] ) );
                    printf( "%d %d %d %d (%d)\n", i, j, a, b,  u[i][j][a][b] );
                }
                printf( "\n" );
            }
            printf( "\n" );
        }
    }
}



int main( int argc, char *argv[] )
{    
    //TO CONVERT COMPLEX INPUT FILE TO INT INPUT FILE, USE:
    /*
        set_vec("input_vec",COMPLEXFILE);   
        set_mat1("input_mat1",COMPLEXFILE);
        set_mat2("input_mat2",COMPLEXFILE);
        save_vect("input_vex_modified", vi);
        save_mat( "input_mat1_modified", u1);
        save_mat( "input_mat2_modified", u2); 
     */
    
    //WORKS ON INT INPUT FILE

    int mode;
    if ( strcmp(argv[4],"int") == 0){
       mode = INTFILE;
    } else {
       mode = COMPLEXFILE;
    }
    set_vec(argv[1],mode);   
    set_mat1(argv[2],mode);
    set_mat2(argv[3],mode);   
    dirac( );
    save_vec( "output_vec" , vo);
    
    //WORKS ON COMPLEX INPUT FILE, BUT USES ONLY THE MAGINFIED, ROUNDED REAL PARTS    
    /*set_vec("input_vec",COMPLEXFILE);   
    set_mat1("input_mat1",COMPLEXFILE);
    set_mat2("input_mat2",COMPLEXFILE);  
    dirac( );
    save_vec( "output_vec" , vo);*/
    
    
    printf( "Mass parameter\n\n" );
    //original line: printf( "m = %d\n", creal( m ) );
    printf( "m = %d\n", m);
    printf( "\nFirst U matrix\n\n" );
    print_u( u1 );
    printf( "\nSecond U matrix\n\n" );
    print_u( u2 );
    printf( "\nInitial vector\n\n" );
    print_vec( vi );
    printf( "\nOutput vector\n\n" );
    print_vec( vo );
    printf( "\n" );

    return 0;
}
