#include "dirac.h"

void step(int i, int j)
{
	//a matrixszorzasok eredmenyei
	number *eredmenya, *eredmenyb,
	       *eredmenyc, *eredmenyd;
	
	//mmel szorzas eredmenye
	number *eredmenym;	

	//osszeadashoz atmeneti eredemnyek
	number *t1, *t2, *t3;
	
	//^ezek^ a valtozok mind 3 hosszu blokk elejet jelzik
	
	//matrix szorzasok kezdete
	number *szorzasa, *szorzasb,
	       *szorzasc, *szorzasd;

	//mmel szorzas kezdete
	number *szorzasm;

	szorzasa = &latt[i][j].addmem[0];
	szorzasb = &latt[i][j].addmem[27];
	szorzasc = &latt[i][j].addmem[54];
	szorzasd = &latt[i][j].addmem[81];

	szorzasm = &latt[i][j].addmem[108];

	eredmenya = &latt[i][j].addmem[113];
	eredmenyb = &latt[i][j].addmem[116];
	eredmenyc = &latt[i][j].addmem[119];
	eredmenyd = &latt[i][j].addmem[122];

	eredmenym = &latt[i][j].addmem[125];
	
	t1 = &latt[i][j].addmem[128];
	t2 = &latt[i][j].addmem[131];
	t3 = &latt[i][j].addmem[134];
	
	int k;

	//t3 + eredmenym -> vn 
	for(k = 0; k < 3; k++)
	{
		plus(t3 +k, eredmenym +k, &latt[i][j].vn[k]);
	}
	//printf("t3 + eredmenym added: %d %d %d, %d %d %d got %d %d %d\n",*t3,*(t3+1),*(t3+2),*eredmenym,*(eredmenym + 1),*(eredmenym + 2),latt[i][j].vn[0],latt[0][j].vn[1],latt[i][j].vn[2]);
	
	//t1 + t2 -> t3 
	for(k = 0; k < 3; k++)
	{
		plus(t1 +k, t2 +k, t3 +k);
	}
	
	//eredmenyc + eredmenyd -> t1 
	for(k = 0; k < 3; k++)
	{
		plus(eredmenyc +k, eredmenyd +k, t2 +k);
	}
	
	//eredmenya + eredmenyb -> t2
	for(k = 0; k < 3; k++)
	{
		plus(eredmenya +k, eredmenyb +k, t1 +k);
	}

	//matrix szorzasok
	matrixProd(latt[(i + 1) % N][j].v, latt[i][j].ua, szorzasa,
		eredmenya, eredmenya +1, eredmenya +2);
	matrixProd(latt[i][(j + 1) % N].v, latt[i][j].ub, szorzasb,
		eredmenyb, eredmenyb +1, eredmenyb +2);
	matrixProd(latt[(i - 1 + N) % N][j].v, latt[i][j].uc, szorzasc,
		eredmenyc, eredmenyc +1, eredmenyc +2);
	matrixProd(latt[i][(j - 1 + N) % N].v, latt[i][j].ud, szorzasd,
		eredmenyd, eredmenyd +1, eredmenyd +2);

	//mmel szorzas
	int m = M;
	parallelProd( &m,
		&latt[i][j].v[0], &latt[i][j].v[1], &latt[i][j].v[2],
		szorzasm, eredmenym, eredmenym +1, eredmenym +2);

	//reseteles
	if( latt[i][j].counter == 0 )
	{
		for(k = 0; k < ADDMEM_LENGTH; k++)
		{
			latt[i][j].addmem[k] = 0;
		}
		latt[i][j].counter = STEP_TIME; 
	}

	k = -1;
	plus(&k, &latt[i][j].counter, &latt[i][j].counter);



}
