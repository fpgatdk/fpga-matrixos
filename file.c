#include "matrix.h"
#include "file.h"

//filebol beolvassa vektort
void readVek( char* name, int mod )
{
    FILE * f;
    f = fopen( name, "r" );
    int i, j, a, i2, j2, a2;
    double re, im;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                switch(mod) {
                    case COMPLEXFILE:
                        fscanf( f, "%d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &re, &im );
                        latt[i2][j2].v[a2] = round(re*MAG);
                        break;
                    case INTFILE:
                        fscanf( f, "%d %d %d (%lf)\n", &i2, &j2, &a2, &re);
                        latt[i2][j2].v[a2] = round(re);
                        break;
                    default:
                        printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                }                         
            }
        }
    }
    fclose( f );
}

//fileba irja vektort
void writeVek( char* name)
{
    FILE * f;
    f = fopen( name, "w" );
    int i, j, a;
    double re; 
    //original line: double im;
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {              
                //original line: im = cimag( vo[i][j][a] ); 
                //orinial line: fprintf( f, "%d %d %d (%d,%d)\n", i, j, a, re, im );
                fprintf( f, "%d %d %d (%d)\n", i, j, a, latt[i][j].v[a] );
            }
        }
    }
    fclose( f );
}

void readMat1( char* name, int mod )
{
    FILE * f;
    int i, j, a, b, i2, j2, a2, b2;
    double re, im; //im IS UNUSED HERE!
    f = fopen( name, "r" );
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                for( b = 0; b < 3; b++ )
                {
                    switch(mod) {
                        case COMPLEXFILE:
                            fscanf( f, "%d %d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &b2, &re, &im );
                            latt[i2][j2].ua[a2][b2] = round(re*MAG);
                            latt[(i2+1)%N][j2].uc[b2][a2] = -round(re*MAG);
                            break;
                        case INTFILE:
                            fscanf( f, "%d %d %d %d (%lf)\n", &i2, &j2, &a2, &b2, &re );
                            latt[i2][j2].ua[a2][b2] = round(re);
                            latt[(i2+1)%N][j2].uc[b2][a2] = -round(re);
                            break;
                        default:
                            printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                    }                                              
                    //original line: u1[i2][j2][a2][b2] = re + I * im;
                    
                }
            }
        }
    }
    fclose( f );
}

void readMat2( char* name, int mod )
{
    FILE * f;
    int i, j, a, b, i2, j2, a2, b2;
    double re, im; //im IS UNUSED HERE!
    f = fopen( name, "r" );
    for( i = 0; i < N; i++ )
    {
        for( j = 0; j < N; j++ )
        {
            for( a = 0; a < 3; a++ )
            {
                for( b = 0; b < 3; b++ )
                {
                    switch(mod) {
                        case COMPLEXFILE:
                            fscanf( f, "%d %d %d %d (%lf,%lf)\n", &i2, &j2, &a2, &b2, &re, &im );
                            latt[i2][j2].ub[a2][b2] = round(re*MAG);
                            latt[i2][(j2+1)%N].ud[b2][a2] = -round(re*MAG);
                            break;
                        case INTFILE:
                            fscanf( f, "%d %d %d %d (%lf)\n", &i2, &j2, &a2, &b2, &re );
                            latt[i2][j2].ub[a2][b2] = round(re);
                            latt[i2][(j2+1)%N].ud[b2][a2] = -round(re);
                            break;
                        default:
                            printf("ERROR! NOT SPECIFIED FILE FORMAT!");
                    }                                              
                    //original line: u1[i2][j2][a2][b2] = re + I * im;
                    
                }
            }
        }
    }
    fclose( f );
}

