CFLAGS=-Wall -g -lm
objects=matrix.o file.o dirac.o
#testdir="$(PWD)/testfiles/"
testdir=testfiles/


all: test1 test2 original

test1: $(objects)

test2: $(objects)

matrix.o: matrix.h

file.o: file.h

dirac.o: file.h matrix.h

clean:
	-rm test2 test1 $(objects) original

test:
	-rm $(testdir)out1 $(testdir)out2
	./test2 $(testdir)$(f1) $(testdir)$(f2) $(testdir)$(f3) $(mode)\
		> $(testdir)out1
	./original $(testdir)$(f1) $(testdir)$(f2) $(testdir)$(f3) $(mode)\
		> $(testdir)out2
	diff -B $(testdir)out1 $(testdir)out2
