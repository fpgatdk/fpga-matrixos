#include <stdio.h>
#include "matrix.h"

void plus (number *a, number *b, number *c)
{
	*c = *a + *b;
}

void shiftl (number *a)
{
	*a = *a << 1;
}

void shiftr (number *a)
{
	*a = *a >> 1;
}

void parallelProd (number *a, number *b1, number *b2, number *b3, number *memstart, number *out1, number *out2, number *out3)
{
	//memoria atnevezes
	//be volt e mar olvasva
	number *beolv = memstart;
	number *c = memstart + 1;
	number *d1 = memstart + 2;
	number *d2 = memstart + 3;
	number *d3 = memstart + 4;

	//osszegzesek
	if(*c & 1)
	{
		plus(out1, d1, out1);
		plus(out2, d2, out2);
		plus(out3, d3, out3);
	}

	//shiftelesek
	shiftr(c);
	shiftl(d1);
	shiftl(d2);
	shiftl(d3);

	//ha meg nem masoltuk be
	if(!(*beolv & 1))
	{
		//bemasolaok
		*c = *a;
		*d1 = *b1;       	
		*d2 = *b2;       	
		*d3 = *b3;
		//jelezzuk mar bemasoltuk
		*beolv = 1;	
	}
}

void matrixProd (vektor v, matrix m, number *memstart,
		number *out1, number *out2, number *out3)
{
	number *szorzas[3];
	number *eredmeny[3][3];
	number *tmp[3];
	
	int i,j;

	for(i = 0; i < 3; i++)
	{
		szorzas[i] = memstart + i*5;
		for(j = 0; j < 3; j++)
		{
			eredmeny[i][j] = memstart + 15 + i*3 + j;
		}
		tmp[i] = memstart + 24 + i;
	}

	plus(tmp[0],eredmeny[2][0],out1);
	plus(tmp[1],eredmeny[2][1],out2);
	plus(tmp[2],eredmeny[2][2],out3);

	plus(eredmeny[0][0],eredmeny[1][0],tmp[0]);
	plus(eredmeny[0][1],eredmeny[1][1],tmp[1]);
	plus(eredmeny[0][2],eredmeny[1][2],tmp[2]);

	for(i = 0; i < 3; i++)
	{
		parallelProd(&v[i], &m[0][i], &m[1][i], &m[2][i], szorzas[i], eredmeny[i][0], eredmeny[i][1], eredmeny[i][2]);
	}
}


