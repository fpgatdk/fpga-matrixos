#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAG 10
#define COMPLEXFILE 1
#define INTFILE 2

//namebol beolvassa a pa.v ertekeket
void readVek(char* name, int mod);

//namebol beolvassa a num indexu matrixokat
void readMat1(char* name, int mod);
//namebol beolvassa a num indexu matrixokat
void readMat2(char* name, int mod);

//kiirja a pak vn erteket nameba
void writeVektor(char* name);
